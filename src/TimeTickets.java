import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


import static java.lang.Integer.parseInt;

public class TimeTickets {

    public static void main(String[] args) {

        LocalDateTime arrivalDate = LocalDateTime.of(0, 1, 1, 0, 0);
        LocalDateTime departmentDate = LocalDateTime.of(0, 1, 1, 0, 0);
        ArrayList<Integer> arrayList = new ArrayList<>();

        try(Scanner scanner = new Scanner(new FileInputStream("E:/Programs/TimeTickets/resources/tickets.json"))){
            String line = scanner.nextLine();
            while(scanner.hasNextLine()) {
                int year, month, day, hour, minute;

                if (line.contains("departure_date")){
                    day = parseInt(line.substring(23,25));
                    month = parseInt(line.substring(26,28));
                    year = 2000 + parseInt(line.substring(29,31));
                    line = scanner.nextLine();
                    if (line.charAt(24) == ':'){
                        hour = parseInt(line.substring(23,24));
                        minute = parseInt(line.substring(25,27));
                    } else {
                        hour = parseInt(line.substring(23,25));
                        minute = parseInt(line.substring(26,28));
                    }
                    departmentDate = LocalDateTime.of(year, month, day, hour, minute);

                    line= scanner.nextLine();

                    day = parseInt(line.substring(21,23));
                    month = parseInt(line.substring(24,26));
                    year = 2000 + parseInt(line.substring(27,29));
                    line = scanner.nextLine();
                    if (line.charAt(22) == ':'){
                        hour = parseInt(line.substring(21,22));
                        minute = parseInt(line.substring(23,25));
                    } else {
                        hour = parseInt(line.substring(21,23));
                        minute = parseInt(line.substring(24,26));
                    }
                    arrivalDate = LocalDateTime.of(year, month, day, hour, minute);

                    long diff = ChronoUnit.MINUTES.between(departmentDate,arrivalDate);
                    arrayList.add((int)diff);
                }

                line = scanner.nextLine();
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }

        long sum = 0;

        for (Integer a: arrayList){
            sum+= a;
        }

        long average = sum / arrayList.size();
        int precenrile = precentile(arrayList,90);
        System.out.println("Среднее время полета между городами Владивосток и Тель-Авив равено "+ average + " минутам.");
        System.out.println("90-й процентиль времени полета между городами Владивосток и Тель-Авив равен " + precenrile + " минутам.");

    }

    public static Integer precentile(ArrayList<Integer> array, double precentile) {
        Collections.sort(array);
        int index = (int) Math.ceil(precentile/100.0 * array.size());
        return array.get(index-1);
    }

}
